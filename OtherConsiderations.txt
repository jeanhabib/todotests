More Tests Possible

Web App
 - Todo List Layout is broken for bigger Descriptions		
 - Problem Editing tasks wtih more than 250 characters.		
 - Duedate not been displayed		
 - Duedate validation		
 - Description Validation		
 - Description field are not been required		
 - Duedate not been required		

Api layer
 - Endpoints status
 - Endpoints CRUD operations
 - Endpoints security 
 - Critical Flows for Tasks and Subtasks Api
 - DB migration specific for tests scenarios (always new, prod sanityzed copy...)
 - mocks


We can maximize the value of the tests by:
 - creating a regression test suit that run everytime a new code is commited to the scm system
 - shifting left the test moment so testers work closer to the developer.
 - ensure the test pyramid is used
 - having end-to-end tests 

We can maximize the value of the product by performing UX research with costumers and developing inovations
We can let collaborators experiment with new technologies
