# ToDoTests
ToDo Tasks App tests with ruby cucumber rspec siteprism capybara selenium-webdriver

## Setup
`install ruby`

run on command terminal
`gem install bundle`

run on repo's root directory
`bundle install`

## Run Tests
`cucumber`

Spot the errors

The drivers will be automatically downloaded when you launch a browser through Selenium by the webdrivers gem.

ToDo Tasks App bug tickets were tagged on .feature files