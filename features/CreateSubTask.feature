Feature: Create Subtask

   As a ToDo App user
   I should be able to create a subtask
   So I can break down my tasks in smaller pieces

Background: Navigate and login to the website
    Given I navigate to the "login" page
    When I insert valid user and password
    Then I must be logged in to the site
    Given I navigate the my tasks page

Scenario: Successfully Create a SubTask
    When I insert a new subtask with "abc" and due date
    Then new subtask must be created

@7342
Scenario: SubTask can’t have more than 250 characters
    When I insert a new subtask with more than 250 characters
    Then new subtask must not be created
