Feature: Create Task

   As a ToDo App user
   I should be able to create a task
   So I can manage my tasks

Background: Navigate and Login to the website
    Given I navigate to the "login" page
    When I insert valid user and password
    Then I must be logged in to the site
    Given I navigate the my tasks page

Scenario: Successfully Create a Task
    When I insert new task with "abc" 
    Then new task must be created

@7348
Scenario: Task should require at least three characters
    When I insert new task with "ab"
    Then new task must not be created

@7339
Scenario: Task can’t have more than 250 characters
    When I insert new task with more than 250 characters
    Then new task must not be created
