Feature: Login to the Site

  I want to be able to login to the website
  In order to interact with mytasks page

  Scenario: Load login page and login to the site
    Given I navigate to the "login" page
    When I insert valid user and password
    Then I must be logged in to the site
    