
When("I insert a new subtask with {string} and due date") do |string|
    @tasks = Tasks.new
    @subtask = SubTask.new
    @tasks.mng_subtask[0].click
    @subtask_size = @subtask.subtask_list.size
    @subtask.new_subtask_description.set string
    @subtask.new_subtask_duedate.set "06/09/2019"
    @subtask.new_subtask_add_btn.click
end

When("I insert a new subtask with more than {int} characters") do |int|
    @tasks = Tasks.new
    @subtask = SubTask.new
    @tasks.mng_subtask[0].click
    @subtask_size = @subtask.subtask_list.size
    @subtask.new_subtask_description.set "s"*(int+1)
    @subtask.new_subtask_duedate.set DateTime.now().strftime("%m/%d/%Y")
    @subtask.new_subtask_add_btn.click
end

Then("new subtask must be created") do
    # binding.pry
    @subtask.has_subtask_list?
    @subtask_size_now = @subtask.subtask_list.size
    expect(@subtask_size_now).to eq(@subtask_size+1)
end

Then("new subtask must not be created") do
    @subtask_size_now = @subtask.subtask_list.size
    expect(@subtask_size_now).to eq(@subtask_size)
end
