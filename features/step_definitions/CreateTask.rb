Given("I navigate the my tasks page") do
    @home = Home.new
    expect(@home.menu).to have_my_tasks_btn
    @home.menu.my_tasks_btn.click
end

When("I insert new task with {string}") do |string|
    @tasks = Tasks.new
    expect(@tasks.menu).to have_my_tasks_btn
    @tasks_size = @tasks.task_list.size
    if string.length < 2
        puts "task com menos de 3 caracteres"
    end
    @tasks.new_task.set "#{string}"
    @tasks.add_task.click
end

When("I insert new task with more than {int} characters") do |int|
    @tasks = Tasks.new
    expect(@tasks.menu).to have_my_tasks_btn
    @tasks_size = @tasks.task_list.size
    @tasks.new_task.set "a"*(int+1)
    @tasks.add_task.click
end

Then("new task must be created") do
    @tasks.has_task_list?
    @tasks_size_now = @tasks.task_list.size
    expect(@tasks_size_now).to eq(@tasks_size+1)
end

Then("new task must not be created") do
    @tasks_size_now = @tasks.task_list.size
    expect(@tasks_size_now).to eq(@tasks_size)
end