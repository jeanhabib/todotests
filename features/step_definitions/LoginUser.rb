Given("I navigate to the {string} page") do |string|
    @login = Login.new
    @login.load
end

When("I insert valid user and password") do    
    @login.user_email.set 'jean.habib@gmail.com'
    @login.user_passw.set '1234567i'
    @login.sigin_btn.click
end

Then("I must be logged in to the site") do
    @home = Home.new
    @home.wait_until_menu_visible(wait: 5)
    expect(@home).to be_displayed
    expect(@home.menu).to have_welcome_msg
end