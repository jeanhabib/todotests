require 'capybara/cucumber'
require 'capybara/rspec'
require 'webdrivers'
require 'selenium-webdriver'
require 'site_prism'
require 'pry'

Capybara.default_driver = :selenium
Capybara.register_driver :selenium do |app|
    Capybara::Selenium::Driver.new(app, :browser => :chrome)
end

Capybara.configure do |config|
    config.default_max_wait_time = 0.1 #=> Wait up to 11 seconds for all querys to fail
    # or alternatively, if you don't want to ever wait
    # config.default_max_wait_time = 0 #=> Don't ever wait! 
end