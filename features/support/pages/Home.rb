class Menu < SitePrism::Section
    element :my_tasks_btn, 'a', text: 'My Tasks' 
    element :sign_in_btn, 'a', text: 'Sign in' 
    element :welcome_msg, 'a', text: 'Welcome, Jean Habib!'
end

class Home < SitePrism::Page
    set_url 'https://qa-test.avenuecode.com'
    section :menu, Menu, '.navbar-default'
end
