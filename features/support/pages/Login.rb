class Login < SitePrism::Page
    set_url 'https://qa-test.avenuecode.com/users/sign_in'
    element :user_email, 'input#user_email'
    element :user_passw, 'input#user_password'
    element :sigin_btn, 'input[type="submit"][value="Sign in"]'
end