class SubTask < SitePrism::Page
    element :new_subtask_description, '#new_sub_task'
    element :new_subtask_duedate, '#dueDate'
    element :new_subtask_add_btn, '#add-subtask'
    element :new_subtask_close_btn, 'button', text: 'Close'
    elements :subtask_list, 'body > div.modal.fade.ng-isolate-scope.in > div > div > div.modal-body.ng-scope > div:nth-child(4) > table > tbody > tr'
end
