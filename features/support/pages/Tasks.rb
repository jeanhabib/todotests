class Menu < SitePrism::Section
    element :my_tasks_btn, 'a', text: 'My Tasks' 
end

class Tasks < SitePrism::Page
    set_url 'https://qa-test.avenuecode.com/tasks'
    element :new_task, '#new_task'
    element :add_task, '[ng-click="addTask()"]'
    elements :task_list, 'tr[ng-repeat="task in tasks"]'
    section :menu, Menu, '.navbar-default'
    elements :mng_subtask, '[ng-click="editModal(task)"]'
end
